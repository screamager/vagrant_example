# vagrant_example
It's test balancing traffics between providers. Implemented on vagrant with using ansible.

dc01 - client  
dg01 - gate  
dp01 - provider 1  
dp02 - provider 2  

dc01:  
   * static ip 192.168.10.11 netmask /24 gateway 192.168.10.10  

dg01:  
   * static ip 192.168.10.10 netmask /24  
   * static ip 192.168.11.10 netmask /24 balance gataway 192.168.11.11  
   * static ip 192.168.12.10 netmask /24 balance gateway 192.168.12.11  

dp01:  
   * static ip 192.168.11.11 netmask /24 masquerade with eth0(vagrant interface nat to host)  

dp02:  
   * static ip 192.168.12.11 netmask /24 masquerade with eth0(vagrant interface nat to host)  

Using vagrant debian image provider virtualbox therefore run:  
```
$ vagrant box add --provider virtualbox --box-version 9.4.0 debian/stretch64
...
$ vagrant up --provider virtualbox  
```
