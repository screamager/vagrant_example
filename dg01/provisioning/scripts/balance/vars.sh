#!/usr/bin/env bash


IPT="iptables"
IPR="ip route"

# LAN interface
IF0="eth1"
# WAN interface 1
IF1="eth2"
# WAN interface 2
IF2="eth3"

IP1="192.168.11.10"
IP2="192.168.12.10"

# LAN netmask
NET0="192.168.10.0/24"
# WAN1 netmask
NET1="192.168.11.0/24"
# WAN2 netmask
NET2="192.168.12.0/24"

# Provider 1 - host: dp01
P1="192.168.11.11"
# Provider 2 - host: dp02
P2="192.168.12.11"

TBL1="provider1"
TBL2="provider2"

# Weight channels
W1="1"
W2="1"
