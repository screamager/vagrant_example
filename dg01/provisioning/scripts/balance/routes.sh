#!/usr/bin/env bash


source /etc/balance/vars.sh

$IPR del default > /dev/null 2>&1;

$IPR add $NET1 dev $IF1 src $IP1 table $TBL1  > /dev/null 2>&1
$IPR add default via $P1 table $TBL1  > /dev/null 2>&1
$IPR add $NET2 dev $IF2 src $IP2 table $TBL2  > /dev/null 2>&1
$IPR add default via $P2 table $TBL2  > /dev/null 2>&1

$IPR add $NET1 dev $IF1 src $IP1  > /dev/null 2>&1
$IPR add $NET2 dev $IF2 src $IP2  > /dev/null 2>&1

$IPR add default via $P1 > /dev/null 2>&1

$IPR add from $IP1 table $TBL1  > /dev/null 2>&1
$IPR add from $IP2 table $TBL2  > /dev/null 2>&1

$IPR add $NET0 dev $IF0 table $TBL1  > /dev/null 2>&1
$IPR add $NET2 dev $IF2 table $TBL1  > /dev/null 2>&1
$IPR add 127.0.0.0/8 dev lo table $TBL1  > /dev/null 2>&1
$IPR add $NET0 dev $IF0 table $TBL2  > /dev/null 2>&1
$IPR add $NET1 dev $IF1 table $TBL2  > /dev/null 2>&1
$IPR add 127.0.0.0/8 dev lo table $TBL2  > /dev/null 2>&1
