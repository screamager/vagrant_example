#!/usr/bin/env bash


echo "1" > /proc/sys/net/ipv4/ip_forward

source /etc/balance/vars.sh

$IPT -t nat -F POSTROUTING
$IPT -t nat -A POSTROUTING -s $NET0 -o $IF1 -j MASQUERADE
$IPT -t nat -A POSTROUTING -s $NET0 -o $IF2 -j MASQUERADE
