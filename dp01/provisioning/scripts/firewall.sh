#!/usr/bin/env bash


echo "1" > /proc/sys/net/ipv4/ip_forward

IPT="iptables"
ETH0="eth0"
ETH1="eth1"

$IPT -t nat -A POSTROUTING -o $ETH0 -j MASQUERADE
$IPT -t nat -A POSTROUTING -o $ETH1 -j MASQUERADE
