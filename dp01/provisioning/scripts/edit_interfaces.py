#!/usr/bin/env python

'''
This's adding line option in section the interface.
'''

from sys import argv

interface, option = argv[1:]
interfaces_file = '/etc/network/interfaces'

with open(interfaces_file, 'r+') as f:
    lines = f.readlines()
    flag = False
    for i in lines:
        if 'iface ' + interface in i:
            flag = True
        if '#VAGRANT-END' in i and flag:
            lines.insert(lines.index(i), option + '\n')
            break
    f.seek(0);
    f.truncate()
    f.write(''.join(lines))
